from django.test import TestCase, Client
from django.urls import resolve
from .views import index, story
from .models import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time



class UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/')
        self.assertFalse(response.status_code == 404)
    
    def test_url_using_status_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        responses = self.client.get("/story/")
        self.assertTemplateUsed(responses, 'story.html')
        response2 = self.client.get("/search/")
        self.assertTemplateUsed(response2, 'search.html')

    def test_url_using_table(self):
        response = Client().get("/search/")
        content = response.content.decode('utf8')
        self.assertIn("table",content) 

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_status_form(self):
        status = Status_Form.objects.create(status = 'FINE')
        counting = Status_Form.objects.all().count()
        self.assertEqual(counting, 1)

    def test_save_a_POST_request(self):
        response = self.client.post('/',
         data={'status' : 'FINE'})
        counting = Status_Form.objects.all().count()
        self.assertEqual(counting, 1)

        self.assertEqual(response.status_code, 200)

        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('FINE', html_response)

class FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        return super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        return super(FunctionalTest, self).tearDown()

    # def test_post(self):
    #     self.browser.get('http://localhost:8000')
    #     text = self.browser.find_element_by_tag_name('h2').text
    #     self.assertIn('HELLO, HOW ARE YOU?', text)
    #     status = self.browser.find_element_by_id('id_status')
    #     submit = self.browser.find_element_by_id('submit')
    #     status.send_keys('FINE')
    #     submit.send_keys(Keys.RETURN)
    
    
    # def test_search(self):
    #     self.browser.get('http://localhost:8000/search/')
    #     search = self.browser.find_element_by_id('search')
    #     search.send_keys('Searching...')
    #     search.send_keys(Keys.ENTER)