from django.urls import path
from . import views

app_name = 'app6'

urlpatterns = [
    path('', views.index, name='index'),
    path('story/', views.story, name='story'),
    path('search/', views.search, name='search'),

]