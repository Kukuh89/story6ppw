from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'accounts'

urlpatterns = [
    # path('', views.indexView, name="home"),
    # path('dashboard/', views.dashboardView, name="dashboard"),
    path('login/', views.user_login, name="login_url"),
    path('register/',views.registerView, name="register_url"),
    path('logout/',views.logout_view, name="logout"),
    path('signout/', views.user_logout, name = "signout"),
]