from django.test import Client, RequestFactory, TestCase
from . import views

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
import unittest
import time
from django.contrib.auth.forms import UserCreationForm
# Create your tests here.

class UnitTest (TestCase):
    def test_url_exists(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/accounts/login/')
        self.assertFalse(response.status_code == 404)

    def test_url_using_accounts_template(self):
        landing = Client().get('/')
        self.assertTemplateUsed(landing, 'index.html')
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
        responses = self.client.get("/accounts/register/")
        self.assertTemplateUsed(responses, 'registration/register.html')
        response3 = self.client.get("/accounts/logout/")
        self.assertTemplateUsed(response3, 'registration/logout.html')

    def test_register_form(self):
        form_data = {
            'username' : 'user',
            'password1' : 'userpassword',
            'password2' : 'userpassword',
        }
        form = UserCreationForm(data = form_data)
        self.assertTrue(form.is_valid())
        request = Client().post('/accounts/register/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_redirect_app6(self):
        response = Client().get('/accounts/dashboard/')


    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='test', password='test_password')

    def test_details(self):
        request = self.factory.get('/login/')
        request.user = self.user
        response = views.user_login(request)
        userlog = authenticate(request.user)
        self.assertEqual(response.status_code, 200)

# class FunctionalTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
#         return super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         return super(FunctionalTest, self).tearDown()

#     def test_login_sukses(self):
#         self.browser.get('http://status6.herokuapp.com/accounts/login/')

#         uname2 = self.browser.find_element_by_id("username")
#         pw2 = self.browser.find_element_by_id("password")
#         btn2 = self.browser.find_element_by_id("login")

#         uname2.send_keys("kukuh")
#         pw2.send_keys("kudidiba199")
#         time.sleep(2)
#         btn2.click()
#         time.sleep(2)

#         self.assertEqual(self.browser.current_url, "http://status6.herokuapp.com/")
        
#         self.assertIn('kukuh', self.browser.page_source)
#         time.sleep(2)

#     def test_logput_sukses(self):
#         self.browser.get('http://status6.herokuapp.com/accounts/logout/')
#         # logoutbtn = self.browser.find_element_by_link_text("logout")
#         # logoutbtn.click()
#         # self.assertEqual(self.browser.current_url, "http://status6.herokuapp.com/logout/")
#         logoutbtn2 = self.browser.find_element_by_id("logout")
#         self.assertIn('<a', self.browser.page_source)
#         logoutbtn2.click()
#         time.sleep(2)
#         self.assertEqual(self.browser.current_url, "http://status6.herokuapp.com/")
#         time.sleep(5)

    # def test_login_dan_logut_tidak_sukses(self):
    #     self.browser.get('http://status6.herokuapp.com/')

    #     uname2 = self.browser.find_element_by_id("uname")
    #     pw2 = self.browser.find_element_by_id("pw")
    #     btn2 = self.browser.find_element_by_id("btn")

    #     uname2.send_keys("Test")
    #     time.sleep(2)
    #     pw2.send_keys("testinisalah")
    #     time.sleep(2)
    #     btn2.click()
    #     time.sleep(2)

    #     self.assertEqual(self.browser.current_url, "http://status6.herokuapp.com/")
    #     self.assertIn('User not found.', self.browser.page_source)

    # def test_login(self):
    #     self.browser.get('http://status6.herokuapp.com/accounts/login')
    #     username = self.browser.find_element_by_id("username")
    #     password = self.browser.find_element_by_id("password")
    #     login = self.browser.find_element_by_id("login")
    #     username.send_keys("kukuh")
    #     password.send_keys("kudidiba199")
    #     login.click()

    # def test_logout(self):
    #     self.browser.get('http://status6.herokuapp.com/accounts/logout')
    #     logout = self.browser.find_element_by_id("logout")
    #     logout.click()


    

        