from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponse

# Create your views here.

response = {}

# def indexView(request):
#     return render(request, 'accountsIndex.html')

# @login_required
# def dashboardView(request):
#     return redirect('app6:index')

def registerView(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('accounts:login_url')
    else:
        form = UserCreationForm()

    return render(request, 'registration/register.html', {'form' : form })

def user_login(request):
    response['username'] = "username"
    if request.method == "POST":
         username = request.POST['username']
         password = request.POST['password']
         user = authenticate(request, username=username, password=password)
         if user is not None:
            login(request, user)
            request.session['username'] = username
            response['username'] = request.session['username']
            return redirect('app6:index')
         else:
            return HttpResponse("Username and Password didn't match")
    else:
        return render(request, 'registration/login.html',response)

def logout_view(request):
    return render(request, 'registration/logout.html')


def user_logout(request):
    request.session.flush()
    logout(request)
    return redirect('app6:index')


    